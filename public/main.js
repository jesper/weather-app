/**
 * No requirements for browsers stated. Assuming IE10 or newer.
 */
var url = urltemplate.parse('api/v1/weather/city/{name}');
var button = document.querySelector('button[type="submit"]');
var input = document.querySelector('input[name="city"]');
var alert = document.querySelector('.notifications .alert');

function setLoadingState() {
  button.classList.add('loading');
  button.querySelector('.loading').removeAttribute('style');
  button.setAttribute('disabled', 'disabled');
}

function setIdleState() {
  button.classList.remove('loading');
  button.removeAttribute('disabled');
}

function showError(text) {
  alert.style.display = 'block';
  alert.querySelector('.text').innerHTML = text;
  alert.querySelector('.close').removeAttribute('style');
}

function hideError() {
  alert.style.display = 'none';
  alert.querySelector('.text').innerHTML = '';
}

function focusInput () {
  input.focus();
  input.setSelectionRange(0, input.value.length);
}

alert.querySelector('.close').addEventListener('click', function () {
  hideError();
});

document.querySelector('form').addEventListener('submit', function (event) {
  event.preventDefault();
  var value = input.value;

  setLoadingState();
  fetch(url.expand({ name: value }))
    .then(function(res) { return res.json(); })
    .then(function(json) {
      if (json === null) {
        showError('City ' + value + ' not found');
      } else {
        history.pushState(json, '', '?city=' + value);
        document.querySelector('#temperature').innerHTML = json ? json.temperature : '';
        document.querySelector('#humidity').innerHTML = json ? json.humidity : '';
        document.querySelector('#wind').innerHTML = json ? json.windText : '';
        document.querySelector('#cityName').innerHTML = value;
        hideError();
      }
    })
    .finally(function () {
      setIdleState();
      focusInput();
    })
});

document.addEventListener('DOMContentLoaded', focusInput);
