const vejrEu = require('./vejrEu')();
const nock = require('nock');

const { lookupCity } = vejrEu;
const fetchMock = nock('http://vejr.eu');

describe('Vejr EU service', () => {
  test('should throw is endpoint doesn\'t return 200', async () => {
    fetchMock
      .get('/api.php')
      .query({
        location: 'Andeby',
        degree: 'C',
      })
      .reply(404);

    await expect(lookupCity('Andeby')).rejects.toThrow('vejr.eu did not return a valid response');
  });

  test('has to return null if no weather report is found for city', async () => {
    fetchMock
      .get('/api.php')
      .query({
        location: 'Not Tibet',
        degree: 'C',
      })
      .reply(200, '<pre>{ "LocationName": "false" }</pre>');

    await expect(lookupCity('Not Tibet')).resolves.toBe(null);
  });

  const testResponseContent = [
    { cityName: 'Bjergkøbing', html: '<pre>{ "CurrentData": { "tag": "pre" }}</pre>', json: { tag: 'pre' }},
    { cityName: 'Springfield', html: '<p>{ "CurrentData": { "tag": "p" }}</p>',       json: { tag: 'p'}},
    { cityName: 'Dalton',      html: '{ "CurrentData": { "tag": false }}',            json: { tag: false}},
  ];
  testResponseContent.forEach((testData) => {
    test('must parse text/html JSON with/without tags', async () => {
      fetchMock
        .get('/api.php')
        .query({
          location: testData.cityName,
          degree: 'C',
        })
        .reply(200, testData.html);

      await expect(lookupCity(testData.cityName)).resolves.toEqual(testData.json);
    });
  });
});
