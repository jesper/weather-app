const fetch = require('node-fetch');
const urlTemplate = require('url-template');

const url = urlTemplate.parse('http://vejr.eu/api.php?location={name}&degree=C');

const lookupCity = async (name) => {
  const response = await fetch(url.expand({ name }), {
    headers: {
      Accept: 'application/json', // API does not respect this
    },
  });

  if (response.status !== 200) {
    throw new Error('vejr.eu did not return a valid response');
  }

  const htmlText = await response.text();
  const jsonStr = htmlText.replace(/^<[^>]+>|<\/[^>]+>$/g, ''); // Strip outer html tags
  const json = JSON.parse(jsonStr);

  return json.CurrentData || null;
};

module.exports = () => {
  return {
    lookupCity,
  };
};
