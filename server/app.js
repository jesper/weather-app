const debug = require('debug')('app');
const express = require('express');
const path = require('path');
const vejrEuService = require('./service/vejrEu');
const Dislocator = require('dislocator');

const app = express();
const locator = new Dislocator();
locator.register('vejrEu', vejrEuService);
app.set('service', locator);

app.set('view engine', 'pug');
app.set('views', path.resolve(__dirname, '..', './views'));
app.use(express.static('public'));

app.use('/api/v1/', require('./middleware/api'));

app.get('/', async (req, res) => {
  debug('Servering request from %s', req.ip);
  const cityName = req.query.city || 'Copenhagen';
  const { lookupCity } = app.get('service').vejrEu;

  debug('Looking up weather for %s', cityName);
  const weather = await lookupCity(cityName);
  debug('Got weather report %O', weather);

  res.render('index', {
    cityName,
    wind: weather && weather.windText,
    humidity: weather && weather.humidity,
    temperature: weather && weather.temperature,
    notFound: weather === null,
  });
});

app.listen(3000, () => console.log('Listening on port 3000!')); // eslint-disable-line no-console
