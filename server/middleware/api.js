const debug = require('debug')('middleware:api');
const express = require('express');
const app = express();

app.get('/weather/city/:city', async (req, res) => {
  const cityName = req.params.city;
  const { lookupCity } = app.get('service').vejrEu;
  debug('Looking up %s', cityName);
  const weather = await lookupCity(cityName);
  debug('Got weather report %O', weather);

  return res.json(weather);
});

module.exports = app;
