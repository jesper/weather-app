# README
This is an experiment which hopefully will evolve into a React SSR application.

## Running the app
To run the application, you need node 8.9 or newer. If you use nvm, you change the version by executing ```nvm use``` from the project root directory.

From there on, simply install the node modules and boot the server. This is done with the commends ```npm i``` and ```npm run start``` in that specific order.

## Development
The project is subject to testing by the Gitlab CI runner, where linting and unit testing is executed. Before pushing your changes, you should run ```npm run lint``` and ```npm run test```. Ideally, all additions to the codebase must have 100% code coverage.

To speed up your feedback loop when developing, you can use ```npm run start:dev```, which will restart the service when you modify files.